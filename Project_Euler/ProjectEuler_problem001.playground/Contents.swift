// PROBLEM 1: Multiples of 3 and 5


// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
// Find the sum of all the multiples of 3 or 5 below 1000.


import UIKit


let maxMulipleSum: Int = 1000
var sumMultiNb: Int = 0
var CheckNumberA: Int = 3
var CheckNumberB: Int = 5


for x in 0..<maxMulipleSum {
  
  if (x % CheckNumberA == 0) || (x % CheckNumberB == 0) {
    sumMultiNb += x
  }
  print(sumMultiNb)
}